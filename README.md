<h1>LAB Project</h1>

This Project is my playground with python ,env build and All of testing function ..etc in here.

此專案為個人python,測試及筆記,使用專案。


## 環境建置(build env)


This playground we dev with python lang, so first you should install python on your system and follow the step to build env.


 ```bash
git clone host:blues.lee/lab_project.git
cd lab_project
virtualenv --no-site-packages <env_name>
pip install -r requirements.txt
source <env_name>/bin/activate
```

Enjoy!!!